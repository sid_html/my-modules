import { Component, Input, OnInit,ViewEncapsulation, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'dynamic-custom-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss'],
  encapsulation: ViewEncapsulation.None

})
export class CheckboxComponent implements OnInit {
  @Input() customBgColor: string;
  @Input() customArrowColor: string;
  @Input() boxType: string;
  @Input() customBorderColor: string;
  @Input() customContent: string;
  @Input() customSize: string;
  @Input() labelContent: string;
  @Input() checkbox:boolean;
  @Input() boxdisabled:boolean;
  @Output() customClick = new EventEmitter();
  checked: boolean = false;
  checkboxDisabled:boolean = false;
  constructor() {
    
  }

  ngOnInit() {
    this.checked = this.checkbox;
    this.checkboxDisabled = this.boxdisabled
  }

  populateLabel(lableText) {
    if(lableText == '' || lableText == undefined ){
      return 'Label';
    } else {
      return lableText;
    }
  }

  change() {
    this.customClick.emit(this.checked);
  }
}
